//
//  Comida.swift
//  Unidad07
//
//  Created by Macbook Pro on 9/17/17.
//  Copyright © 2017 Macbook Pro. All rights reserved.
//

import Foundation

struct Comida {
    // TODO: Descomentar la linea de abajo
    // let formatter = DateFormatter()
    var hora : Date
    var descripcion: String
    var creditos: Double
    
    init (hora: Date, descripcion: String, creditos: Double) {
        self.hora = hora
        self.descripcion = descripcion
        self.creditos = creditos
        // TODO: Descomentar la linea de abajo
        //formatter.timeStyle = .short

    }
    
    var title: String {
        get {
            // TODO: Comentar la linea de abajo
            return "\(self.hora) - \(self.descripcion)"
            // TODO: Desomentar la linea de abajo
            // return "\(formatter.string(from: self.hora)) - \(self.descripcion)"
        }
    }
}
